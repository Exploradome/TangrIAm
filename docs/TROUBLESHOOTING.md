# Troubleshooting

## General
If it doesn't start, carefully read the error messages.
For example, you might be using the wrong camera id (check the config.ini file to change that)

## ModuleNotFoundError: No module named 'tensorflow'
If Python doesn't find packages like Tensorflow, VLC, there may be a misconfiguration between Python and PIP
Type ```python --version``` and ```pip --version```.
Both should mention the same Python version (ie: Python 3.8)

If not, go to Microsoft Settings=>Applications=>Alias (or search for alias in the search bar) and activate :
- Python 3.8 (python.exe)
- Pip 3.8 (pip.exe)

## Could not load dynamic library 'cudart64_101.dll'; dlerror: cudart64_101.dll not found

## ValueError: Unable to open self.video source
No webcam has been plugged or recognized by Windows.
Check connectivity and driver installation.
Try opening the Windows Camera app to check if the camera is detected.