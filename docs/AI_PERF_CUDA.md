# Improve AI performance
To improve prediction execution speed, you can use an NVidia graphics card and the CUDA toolkit.

The following versions work with Tensorflow 2.3.0 (which is used in the app) and windows 10

## CUDA Toolkit 10.1 update 2 (cuda_10.1.243_426.00_win10.exe)
Download and install the file from https://developer.nvidia.com/cuda-10.1-download-archive-update2?target_os=Windows&target_arch=x86_64&target_version=10&target_type=exelocal

## cuDNN 7.6.5 for CUDA 10.1 (cudnn-10.1-windows10-x64-v7.6.5.32.zip)
Download the file from https://developer.nvidia.com/rdp/cudnn-archive (registration required)
or https://developer.nvidia.com/compute/machine-learning/cudnn/secure/7.6.5.32/Production/10.1_20191031/cudnn-10.1-windows10-x64-v7.6.5.32.zip

Unzip the content and copy the folders "bin"/"lib"/"include" in :
```c:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1```

## Performance benchmark 

### Surface Book 2: i7-8650U / NVidia GeForce GTX 1050
| Activity       | With CUDA      | Without CUDA|
| -------------- | --------------:| --------------:|
| Preprocessing  |  10ms / >25FPS |  10ms / >25FPS |
| Classification | 150ms /  ~6FPS | 300ms /  3 FPS |
