# Keyboard shortcuts for testing and calibration
- **0 - 2**: switch to other cameras (0 = first default camera on the operation system)
- **b / B**: change camera brightness
- **c / C**: change camera contrast
- **e / E**: change camera exposure
- **f / F**: change camera focus
- **s**: save camera settings in the config.ini (camera id, brightness, config, exposure, focus, camera crop)
- **S**: Save the camera feed on disk (camera_raw, camera_left, camera_right)
- **Enter**: switch to fullscreen, remove the title bar and move to the top left of the screen
- **!**  : stretch to fullscreen
- **q**: quit the application (only for Windows)
- **u, i, o**: simulate button 1 2 3 for right(top) player
- **k, j, h**: simulate button 1 2 3 for left(bottom) player
- **v**: use a fake camera image (camera_mock.jpg)
- **w**: use a fake camera feed (camera__mock.mp4)
- **V**: display a visual feedback of what the camera sees
- **Arrows**: adjust camera crop coordinates
  - Top: use Up/Down arrows
  - Left: use Left/Right arrows
  - Bottom: use Shift+Up/Down arrows
  - Right: use Shift+Left/Right arrows

# Settings and .INI files
TangrIAm app uses a config.ini file to adjust various settings regarding:
- gameplay (duration of the game, ...)
- camera adjustments (focus, exposure, crop, ...)
- IA settings (model, ...)
- UI elements (text, placement...)

# Multilanguage
For generic content and settings, there is media directory and a global configuration file.
For each language, there is a dedicated media directory and config file. 
```
root directory/
│
├── config.ini            Global config file with generic settings
│
└── media/                Media directories with generic content
    ├── de                Localized content (media directory and config) for German
    │   └── config.ini    
    │
    ├── en                Localized content (media directory and config) for English
    │   └── config.ini    
    │
    └── fr                Localized content (media directory and config) for French
        └── config.ini    
```

## Global config.ini
### UI elements
- languages=fr,en,de : list of languages using their directory name (this order will be used to display the language menu)
- folder=media: name of the media directory
- width / height : application size (should match screen resolution for fullscreen)
- mask : mask image for application decoration
 
### Gameplay
- GAME_DURATION: duration of one game
- INACTIVITY_TIME_LIMIT: delay after which the application go back to the main menu

### Camera settings
- ID: Values are from 0 to N representing the camera ID. 0 is the default main camera. 
- FOCUS: 
- CONTRAST: 
- BRIGHTNESS: 
- EXPOSURE: 

### IA settings
- Model: name of the AI model
- PREDICT_PS: number of predictions per second
- LABELS: names of all possible labels in the AI model

## Localized config.ini for each language

### UI elements
- (all images and masks)
- (all texts)
- (coordinates, font, size, color)
