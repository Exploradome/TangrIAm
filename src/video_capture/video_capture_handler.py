import cv2
import numpy as np
import time

from form_classification.gradcam import GradCam
from form_classification.image_processor import ImageProcessor
from form_classification.model import get_top_n_predictions

from form_classification.gradcam import GradCam

from video_capture.camera_handler import CameraHandler

from ui.ui_data import UIData


class VideoCaptureHandler:
    def __init__(self, model, never_predicts: bool, number_of_top_predictions: int,
                 video_recorder: CameraHandler,
                 im_width: int,
                 im_height: int,
                 contrast, brightness, config
                 ):
        self.config = config

        self.im_height = im_height
        self.im_width = im_width

        self.contrast = contrast
        self.brightness = brightness

        self.video_recorder = video_recorder
        self.model = model
        self.gradcam = GradCam(model)
        self.classifier_layers = ['global_average_pooling2d', 'dense']
        self.display = {'all': None,
                        'left': [None, (None, None, None, None), None],
                        'right': [None, (None, None, None, None), None]}
        self.left_top_n_predictions = None
        self.left_explanation = None
        self.right_top_n_predictions = None
        self.right_explanation = None
        self.camera_invert_colors = config.getboolean('INVERT_COLORS')
        self.camera_crop_top = int(config['CAMERA_CROP_TOP'])
        self.camera_crop_bottom = int(config['CAMERA_CROP_BOTTOM'])
        self.camera_crop_left = int(config['CAMERA_CROP_LEFT'])
        self.camera_crop_right = int(config['CAMERA_CROP_RIGHT'])
        self.left_playarea_offset_x = int(config['LEFT_PLAYAREA_OFFSET_X'])
        self.left_playarea_offset_y = int(config['LEFT_PLAYAREA_OFFSET_Y'])
        self.right_playarea_offset_x = int(config['RIGHT_PLAYAREA_OFFSET_X'])
        self.right_playarea_offset_y = int(config['RIGHT_PLAYAREA_OFFSET_Y'])
        self.number_of_top_predictions = number_of_top_predictions
        self.fake_camera = config.getboolean('FAKE_CAMERA')
        self.fake_camera_video = config.getboolean('FAKE_CAMERA_VIDEO')
        self.fake_camera_video_cap = None
        self.fake_camera_video_fps = 0
        self.fake_camera_video_last_ts = 0
        self.never_predicts = never_predicts
        self.width = 0
        self.half_width = 0
        self.height = 0
        self.left_crop_dimensions = (0, 0, 0, 0)
        self.right_crop_dimensions = (0, 0, 0, 0)

        self.playarea_top = 0
        self.playarea_bottom = 0
        self.playarea_left = 0
        self.playarea_right = 0
        
        self.must_save_next_frame = False

    def compute_crop(self, ui: UIData):
        self.width = int(self.video_recorder.width)
        self.half_width = self.width // 2
        self.height = int(self.video_recorder.height)
        camera_screen_h_ratio = (self.width - self.camera_crop_left - self.camera_crop_right) / ui.width
        camera_screen_v_ratio = (self.height - self.camera_crop_top - self.camera_crop_bottom) / ui.height
        self.playarea_top = int(ui.l.playarea_top * camera_screen_h_ratio)
        self.playarea_bottom = int(ui.l.playarea_bottom * camera_screen_h_ratio)
        self.playarea_left = int(ui.l.playarea_left * camera_screen_v_ratio)
        self.playarea_right = int(ui.l.playarea_right * camera_screen_v_ratio)
        self.left_crop_dimensions = (
            self.camera_crop_top + self.playarea_left + self.left_playarea_offset_y,
            self.height - self.camera_crop_bottom - self.playarea_right + self.left_playarea_offset_y,
            self.camera_crop_left + self.playarea_bottom + self.left_playarea_offset_x,
            (self.width + self.camera_crop_left - self.camera_crop_right)//2 - self.playarea_top + self.left_playarea_offset_x)
        self.right_crop_dimensions = (
            self.camera_crop_top + self.playarea_left + self.right_playarea_offset_y,
            self.height - self.camera_crop_bottom - self.playarea_right + self.right_playarea_offset_y,
            (self.width + self.camera_crop_left - self.camera_crop_right)//2 + self.playarea_top + self.right_playarea_offset_x,
            self.width - self.camera_crop_right - self.playarea_bottom + self.right_playarea_offset_x)

    def adjust_crop(self, ui: UIData, l, t, r, b, lx, ly, rx, ry):
        self.camera_crop_left += l
        self.camera_crop_top += t
        self.camera_crop_right += r
        self.camera_crop_bottom += b
        self.left_playarea_offset_x += lx
        self.left_playarea_offset_y += ly
        self.right_playarea_offset_x += rx
        self.right_playarea_offset_y += ry
        print(f'CROP = {(self.camera_crop_left,self.camera_crop_top,self.camera_crop_right,self.camera_crop_bottom)}')
        self.compute_crop(ui)

    def camera_warm_up(self):
        if not self.fake_camera and not self.fake_camera_video:
            print("Camera warm up")
            self.video_recorder.open_camera()
            _, _ = self.video_recorder.vid.read()
    
    def ai_warm_up(self):
        if not self.never_predicts:
            print("AI warm up")
            get_top_n_predictions(np.zeros((self.im_height, self.im_width, 3), np.uint8), self.model, 1)

    def save_camera_feed(self):
        self.must_save_next_frame = True
    
    def get_display_content(self, must_predict: bool, game_mode) -> dict:
        feed_saved = False
        frame = None
        if self.fake_camera:
            frame = cv2.imread('../media/mock/camera_mock.jpg')
        elif self.fake_camera_video:
            if self.fake_camera_video_cap is None:
                self.fake_camera_video_cap = cv2.VideoCapture('../media/mock/camera_mock.mp4') 
                self.fake_camera_video_fps = 1/self.fake_camera_video_cap.get(cv2.CAP_PROP_FPS)
            if time.time() - self.fake_camera_video_last_ts < self.fake_camera_video_fps:
                self.fake_camera_video_cap.set(cv2.CAP_PROP_POS_FRAMES, self.fake_camera_video_cap.get(cv2.CAP_PROP_POS_FRAMES) - 1)
            else:
                self.fake_camera_video_last_ts = time.time()
            ret, frame = self.fake_camera_video_cap.read()
            if not ret:
                self.fake_camera_video_cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
        else:
            if self.video_recorder.vid is None:
                self.video_recorder.open_camera()
            if self.video_recorder.vid.isOpened():
                _, frame = self.video_recorder.vid.read()
        if frame is not None:
            if self.must_save_next_frame:
                feed_saved = True
                cv2.imwrite('../camera_raw.jpg', frame)
                cv2.imwrite('../camera_cropped.jpg', 
                            frame[
                                  self.camera_crop_top:self.height - self.camera_crop_bottom,
                                  self.camera_crop_left:self.width - self.camera_crop_right
                                  ])
            if self.camera_invert_colors:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            #self.display['all'] = frame[
            #                      self.camera_crop_top:self.height - self.camera_crop_bottom,
            #                      self.camera_crop_left:self.width - self.camera_crop_right
            #                      ]
            
            if game_mode == 'left' or game_mode == 'both':
                cropped_left_frame = frame[
                                     self.left_crop_dimensions[0]:self.left_crop_dimensions[1],
                                     self.left_crop_dimensions[2]:self.left_crop_dimensions[3]]
                                     
                if self.must_save_next_frame:
                    cv2.imwrite('../camera_left.jpg', cropped_left_frame)
                self.display['left'][0] = cropped_left_frame
                self.display['left'][1] = self.process_frame_and_predict(cropped_left_frame,
                                                                         n=self.number_of_top_predictions,
                                                                         rotation=cv2.ROTATE_90_COUNTERCLOCKWISE,
                                                                         must_predict=must_predict)

            if game_mode == 'right' or game_mode == 'both':
                cropped_right_frame = frame[
                                      self.right_crop_dimensions[0]:self.right_crop_dimensions[1],
                                      self.right_crop_dimensions[2]:self.right_crop_dimensions[3]]

                if self.must_save_next_frame:
                    cv2.imwrite('../camera_right.jpg', cropped_right_frame)
                self.display['right'][0] = cropped_right_frame
                self.display['right'][1] = self.process_frame_and_predict(cropped_right_frame,
                                                                          n=self.number_of_top_predictions,
                                                                          rotation=cv2.ROTATE_90_CLOCKWISE,
                                                                          must_predict=must_predict)
        if feed_saved: 
            self.must_save_next_frame = False
        return self.display

    def process_frame_and_predict(self, frame: np.ndarray, n: int, rotation, must_predict: bool):
        image_processor = ImageProcessor(frame, self.camera_invert_colors, self.im_width, self.im_height)
        image_processor.process_image()

        # Clear prediction cache
        if image_processor.image_to_classify is None:
            if rotation == cv2.ROTATE_90_COUNTERCLOCKWISE:
                self.left_top_n_predictions = None
                self.left_explanation = None
            else:
                self.right_top_n_predictions = None
                self.right_explanation = None

        if image_processor.image_to_classify is not None and must_predict and self.model is not None:
            rotated_image = cv2.rotate(image_processor.image_to_classify, rotation)
            top_n_predictions, predictions = get_top_n_predictions(rotated_image, self.model, n)
            explanation = self.explain_prediction(predictions, rotated_image)

            # cache predictions between predict
            if rotation == cv2.ROTATE_90_COUNTERCLOCKWISE:
                self.left_top_n_predictions = top_n_predictions
                self.left_explanation = explanation
            else:
                self.right_top_n_predictions = top_n_predictions
                self.right_explanation = explanation

        if rotation == cv2.ROTATE_90_COUNTERCLOCKWISE:
            return image_processor.image_with_bounding_box, image_processor.segmented_and_cropped_image, \
                   self.left_explanation, self.left_top_n_predictions
        else:
            return image_processor.image_with_bounding_box, image_processor.segmented_and_cropped_image, \
                   self.right_explanation, self.right_top_n_predictions

    def explain_prediction(self, predictions, image: np.ndarray) -> np.ndarray:
        heatmap = self.gradcam.compute_heatmap(image.reshape(1, image.shape[0], image.shape[1], 3),
                                               np.argmax(predictions[0]))

        overlay = self.gradcam.overlay_heatmap(heatmap, image, alpha=0.2)
        overlay = cv2.cvtColor(overlay, cv2.COLOR_BGR2RGB)
        return overlay
