import sys
import time
import tkinter as tk
from os.path import  join as joined

import vlc
import platform


class VideoReader:

    def __init__(self, angle: int):
        self.angle = angle
        self.mute = False
        self.video_container = None
        self.is_MacOS = platform.system().lower().startswith('darwin')
        self.is_Windows = platform.system().lower().startswith('win')
        self.is_Linux = platform.system().lower().startswith('linux')

        if self.is_MacOS:
            from ctypes import c_void_p, cdll
            # libtk = cdll.LoadLibrary(ctypes.util.find_library('tk'))
            # returns the tk library /usr/lib/libtk.dylib from macOS,
            # but we need the tkX.Y library bundled with Python 3+,
            # to match the version number of tkinter, _tkinter, etc.
            try:
                libtk = 'libtk%s.dylib' % (tk.TkVersion,)
                prefix = getattr(sys, 'base_prefix', sys.prefix)
                libtk = joined(prefix, 'lib', libtk)
                dylib = cdll.LoadLibrary(libtk)
                # getNSView = dylib.TkMacOSXDrawableView is the
                # proper function to call, but that is non-public
                # (in Tk source file macosx/TkMacOSXSubwindows.c)
                # and dylib.TkMacOSXGetRootControl happens to call
                # dylib.TkMacOSXDrawableView and return the NSView
                self.GetNSView = dylib.TkMacOSXGetRootControl
                # C signature: void *_GetNSView(void *drawable) to get
                # the Cocoa/Obj-C NSWindow.contentView attribute, the
                # drawable NSView object of the (drawable) NSWindow
                self.GetNSView.restype = c_void_p
                self.GetNSView.argtypes = c_void_p,
                del dylib

            except (NameError, OSError):  # image or symbol not found
                def _GetNSView(unused):
                    return None
        self.instance = None
        self.player = None

    def prepare_video(self, video_path, video_container, mute):
        self.mute = mute
        if self.angle != 0:
            args = [f'--video-filter=transform{{type={self.angle}}}']
        else:
            args = []
        if self.mute:
            args.append('--noaudio')
        if self.is_Linux:
            args.append('--no-xlib')
        self.instance = vlc.Instance(args)
        self.player = self.instance.media_player_new()
        self.video_container = video_container
        print(f'Starting Video Mute:{mute} {video_path}')
        media = self.instance.media_new(video_path)
        media.parse_with_options(vlc.MediaParseFlag.local, -1)
        self.player.set_media(media)
        while media.get_duration() < 0:
            time.sleep(0.05)
        duration = media.get_duration()
        return duration

    def play_video(self):
        # set the window id where to render VLC's video output
        window_id = self.video_container.winfo_id()
        if self.is_Windows:
            self.player.set_hwnd(window_id)       # causes the video to play in the entire panel on
        # macOS, covering the buttons, sliders, etc.
        # XXX 2) .winfo_id() to return NSView on macOS?
        # v = self.GetNSView(window_id)
        # if v:
        #    self.player.set_nsobject(v)
        # else:
        #    self.player.set_xwindow(window_id)  # plays audio, no video
        self.player.play()

    def set_volume(self, volume):
        self.player.audio_set_volume(volume)

    def stop(self):
        self.player.stop()
        while self.player.is_playing():
            print(self.player.is_playing())
            time.sleep(0.05)
        self.player.release()
        self.player = None
