from Phidget22.Devices.DigitalInput import *
from Phidget22.Devices.DigitalOutput import *

from ui.ui_data import UIData, Side


class PhidgetManager:

    def __init__(self, ui: UIData):
        self.phidget_available = False
        self.ui = ui
        try:
            self.buttons = \
                [
                    [DigitalInput(), DigitalInput(), DigitalInput()],
                    [DigitalInput(), DigitalInput(), DigitalInput()]
                ]
            self.leds = \
                [
                    [DigitalOutput(), DigitalOutput(), DigitalOutput()],
                    [DigitalOutput(), DigitalOutput(), DigitalOutput()]
                ]

            for side_idx, button_by_side in enumerate(self.buttons):
                for btn_idx, button in enumerate(button_by_side):
                    button.setChannel(side_idx * 3 + btn_idx)
                    button.setOnStateChangeHandler(self.on_state_change)
                    button.openWaitForAttachment(5000)

            for side_idx, leds_by_side in enumerate(self.leds):
                for led_idx, led in enumerate(leds_by_side):
                    led.setChannel(side_idx * 3 + led_idx)
                    led.openWaitForAttachment(5000)
            self.phidget_available = True
        except PhidgetException as e:
            print(f'Phidget not available: {e.details}')
        except OSError:
            print('OSError : no Phidget image found')

    def on_state_change(self, button, state):
        if state == 1:
            channel = button.getChannel()
            side = self.ui.left if channel <= 2 else self.ui.right
            channel = channel % 3
            self.ui.do_button_command(side, channel)

    def set_leds_status(self, side: Side):
        if self.phidget_available:
            side_idx = 0 if side.is_left else 1
            for i, cmd in enumerate(side.button_commands):
                self.leds[side_idx][i].setDutyCycle(0 if cmd is None else 1)

    def destroy(self):
        print("Turn off Phidgets")
        if self.phidget_available:
            for button_by_side in self.buttons:
                for button in button_by_side:
                    button.close()

            for leds_by_side in self.leds:
                for led in leds_by_side:
                    led.close()

    def __del__(self):
        self.destroy()
