from ui.native_ui import NativeUI

if __name__ == '__main__':
    import configparser

    config = configparser.ConfigParser()
    config.read('../config.ini', "utf8")
    ui = NativeUI(config)
    ui.init_ai()
    ui.set_keyboard_bindings()
    ui.init_ui()
    ui.language_menu()
    ui.run_loop()
