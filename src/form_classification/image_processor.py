import cv2
import numpy as np


class ImageProcessor:
    def __init__(self, image: np.ndarray, invert_colors: bool, im_width: int, im_height: int):
        self.image = image

        self.invert_colors = invert_colors

        self.im_height = im_height
        self.im_width = im_width

        self.image_with_bounding_box = None
        self.segmented_and_cropped_image = None
        self.image_to_classify = None

    def process_image(self, min_cropped_img_shape=50):
        if self.invert_colors:
            gray = cv2.cvtColor(self.image, cv2.COLOR_RGB2GRAY)
        else:
            gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)

        segmented_img_grayscale = self.segment_image(gray)
        segmented_img_rgb = cv2.cvtColor(segmented_img_grayscale, cv2.COLOR_GRAY2RGB)
        segmented_img_rgb_plus_box = segmented_img_rgb.copy()
        countours = self.draw_bounding_box(segmented_img_grayscale, gray, segmented_img_rgb_plus_box, 2000, 300000)
        outer = ImageProcessor.get_outer_box(countours)
        if outer is not None:
            x, y, w, h = outer
            self.image_with_bounding_box = self.image.copy()
            ImageProcessor.draw_rectangle(self.image_with_bounding_box, x, y, w, h)

            largest_side = w if w > h else h
            center_x = x + w // 2
            center_y = y + h // 2
            self.segmented_and_cropped_image = segmented_img_rgb_plus_box[
                                               max(0, center_y - largest_side // 2): min(center_y + largest_side // 2, segmented_img_rgb_plus_box.shape[1]),
                                               max(0, center_x - largest_side // 2): min(center_x + largest_side // 2, segmented_img_rgb_plus_box.shape[0])]
            if self.segmented_and_cropped_image.size == 0:
                self.segmented_and_cropped_image = None

            image_to_classify = segmented_img_rgb[y:y + h, x:x + w]
            if image_to_classify.shape[0] > min_cropped_img_shape and \
                    image_to_classify.shape[1] > min_cropped_img_shape:
                self.image_to_classify = cv2.resize(image_to_classify, (self.im_height, self.im_width))
            else:
                self.image_to_classify = None
        else:
            self.image_with_bounding_box = self.image.copy()
            self.segmented_and_cropped_image = None
            self.image_to_classify = None

    @staticmethod
    def segment_image(image: np.ndarray) -> np.ndarray:
        # blurred = cv2.GaussianBlur(image, (5, 5), 0)
        _, segmented_img = cv2.threshold(image, 128, 255, cv2.THRESH_BINARY_INV)
        return segmented_img

    @staticmethod
    def get_outer_box(contours):
        merged = None
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            if merged is None:
                merged = (x, y, w, h)
            else:
                merged = ImageProcessor.union(merged, (x, y, w, h))
        return merged

    @staticmethod
    def draw_bounding_box(segmented_img_src, gray, rgb, min_rectangle_area, max_rectangle_area) -> tuple:
        contours = ImageProcessor.find_contours(segmented_img_src)
        selected = []
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            if min_rectangle_area < w * h < max_rectangle_area:
                selected.append(cnt)
                if gray is not None:
                    ImageProcessor.draw_rectangle(gray, x, y, w, h)
                if rgb is not None:
                    ImageProcessor.draw_rectangle(rgb, x, y, w, h)
        return selected

    @staticmethod
    def union(a, b):
        x = min(a[0], b[0])
        y = min(a[1], b[1])
        w = max(a[0] + a[2], b[0] + b[2]) - x
        h = max(a[1] + a[3], b[1] + b[3]) - y
        return x, y, w, h

    @staticmethod
    def draw_rectangle(image, x, y, w, h):
        image = cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        return image

    @staticmethod
    def find_contours(segmented_img: np.ndarray) -> list:
        contours, _ = cv2.findContours(segmented_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        return contours

    @staticmethod
    def resize_images(img, image_width, image_height):
        if img is not None:
            img = cv2.resize(img, (image_height, image_width))
        return img
