import codecs
import ctypes
from tkinter import Tk, Label, Canvas, NW, LEFT, RIGHT, Button, E, W, CENTER, SW
from typing import Optional
import numpy as np
from PIL import Image, ImageTk
import os
import platform
import time

from infrastructure.video_reader import VideoReader
from ui.arc_drawing_handler import ArcDrawingHandler
from ui.bar_plot_drawing_handler import BarPlotDrawingHandler
from ui.ui_data import UIData, Side
from video_capture.video_capture_handler import VideoCaptureHandler
from video_capture.camera_handler import CameraHandler
from form_classification.image_processor import ImageProcessor
from form_classification.model import get_trained_model

from infrastructure.phidget_manager import PhidgetManager


def add_trailing_space(text):
    if text is None:
        return None
    else:
        return text + ' '


class NativeUI:
    def __init__(self, config):
        self.overrideredirect = 0
        self.config = config

        self.model_path = os.path.join(config['AI']['MODEL_PATH'], config['AI']['MODEL_NAME'])
        self.labels = [label.strip() for label in self.config['AI']['LABELS'].split(',')]
        self.channels = int(self.config['AI']['CHANNELS'])
        self.cv_width = int(self.config['AI']['IMAGE_WIDTH'])
        self.cv_height = int(self.config['AI']['IMAGE_HEIGHT'])
        self.cv_contrast = float(self.config['AI']['CONTRAST'])
        self.cv_brightness = float(self.config['AI']['BRIGHTNESS'])
        self.number_of_top_predictions = int(self.config['AI']['NUMBER_OF_TOP_PREDICTIONS'])
        if float(self.config['AI']['PREDICT_PS']) < 1:
            self.never_predicts = True
            self.delay_between_predicts = 0
        else:
            self.never_predicts = False
            self.delay_between_predicts = int(1000 / float(self.config['AI']['PREDICT_PS']))
        self.last_predict_ts = 0

        self.inaction_time_limit = int(self.config['GAMEPLAY']['INACTIVITY_TIME_LIMIT'])
        self.game_duration = int(self.config['GAMEPLAY']['GAME_DURATION'])
        self.delay_between_frames = int(1000 / 20)
        self.update_on = False
        self.feedback_camera = False

        self.window = Tk()
        self.window.call('tk', 'scaling', 1)
        # eliminate the title bar
        # Only works for Windows
        if platform.system().lower().startswith('win'):
            ctypes.windll.shcore.SetProcessDpiAwareness(2)
        self.switch_fullscreen()
        self.window.config(cursor="none")
        self.ui: Optional[UIData] = None
        self.barchart_drawing_handler: Optional[BarPlotDrawingHandler] = None
        self.arc_drawing_handler: Optional[ArcDrawingHandler] = None
        self.model = None
        self.camera: Optional[CameraHandler] = None
        self.video_capture_handler: Optional[VideoCaptureHandler] = None
        self.game_over: bool = False
        self.game_start_time: Optional[float] = None
        self.pause_start_time: Optional[float] = None
        self.inaction_id: Optional[str] = None
        self.video_timeout_id: Optional[str] = None
        self.update_display_id: Optional[str] = None
        self.phidget_manager: Optional[PhidgetManager] = None

        self.max_countdown_index: int = 0
        self.countdown_index: int = 0

    def init_ai(self):
        if not self.never_predicts:
            print('Loading AI model...')
            self.model = get_trained_model(self.model_path)

        print('Starting camera...')
        self.camera = CameraHandler(camera_id=int(self.config['CAMERA']['ID']),
                                    fps=int(self.config['CAMERA']['FPS']),
                                    width=int(self.config['CAMERA']['WIDTH']),
                                    height=int(self.config['CAMERA']['HEIGHT']),
                                    exposure=float(self.config['CAMERA']['EXPOSURE']),
                                    contrast=int(self.config['CAMERA']['CONTRAST']),
                                    brightness=int(self.config['CAMERA']['BRIGHTNESS']),
                                    focus=int(self.config['CAMERA']['FOCUS']))

        self.video_capture_handler = VideoCaptureHandler(self.model, self.never_predicts,
                                                         self.number_of_top_predictions,
                                                         self.camera,
                                                         self.cv_width,
                                                         self.cv_height,
                                                         self.cv_contrast,
                                                         self.cv_brightness,
                                                         self.config['CAMERA'])
        self.video_capture_handler.camera_warm_up()
        self.video_capture_handler.ai_warm_up()

    def destroy(self):
        if self.inaction_id is not None:
            self.window.after_cancel(self.inaction_id)
        if self.video_timeout_id is not None:
            self.window.after_cancel(self.video_timeout_id)
        self.window.destroy()
        self.camera.destroy()
        self.phidget_manager.destroy()

    def set_keyboard_bindings(self):
        self.window.bind("<Return>", lambda event: self.switch_fullscreen())

        if platform.system().lower().startswith('win'):
            self.window.bind('!',
                             lambda event: self.window.attributes('-fullscreen',
                                                                  not self.window.attributes('-fullscreen')))
        else:
            self.window.bind('!',
                             lambda event: self.window.attributes('-zoomed',
                                                                  not self.window.attributes('-zoomed')))
        self.window.bind('q', lambda event: self.destroy())
        self.window.bind('s', lambda event: self.save_config())
        self.window.bind('S', lambda event: self.save_camera_feed())
        self.window.bind('E', lambda event: self.camera.increase_exposure())
        self.window.bind('e', lambda event: self.camera.decrease_exposure())
        self.window.bind('B', lambda event: self.camera.increase_brightness())
        self.window.bind('b', lambda event: self.camera.decrease_brightness())
        self.window.bind('C', lambda event: self.camera.increase_contrast())
        self.window.bind('c', lambda event: self.camera.decrease_contrast())
        self.window.bind('F', lambda event: self.camera.increase_focus())
        self.window.bind('f', lambda event: self.camera.decrease_focus())
        self.window.bind('0', lambda event: self.camera.set_camera_id(0))
        self.window.bind('1', lambda event: self.camera.set_camera_id(1))
        self.window.bind('2', lambda event: self.camera.set_camera_id(2))

        self.window.bind('k', lambda event: self.ui.do_button_command(self.ui.left, 0))
        self.window.bind('j', lambda event: self.ui.do_button_command(self.ui.left, 1))
        self.window.bind('h', lambda event: self.ui.do_button_command(self.ui.left, 2))

        self.window.bind('u', lambda event: self.ui.do_button_command(self.ui.right, 0))
        self.window.bind('i', lambda event: self.ui.do_button_command(self.ui.right, 1))
        self.window.bind('o', lambda event: self.ui.do_button_command(self.ui.right, 2))

        self.window.bind('v', lambda event: self.switch_fake_camera())
        self.window.bind('w', lambda event: self.switch_fake_camera_video())
        self.window.bind('V', lambda event: self.switch_feedback_camera())
        self.window.bind('<Left>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 1, 0, 0, 0, 0, 0, 0, 0))
        self.window.bind('<Right>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, -1, 0, 0, 0, 0, 0, 0, 0))
        self.window.bind('<Up>', lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 1, 0, 0, 0, 0, 0, 0))
        self.window.bind('<Down>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, -1, 0, 0, 0, 0, 0, 0))
        self.window.bind('<Shift-KeyPress-Left>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 1, 0, 0, 0, 0, 0))
        self.window.bind('<Shift-KeyPress-Right>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, -1, 0, 0, 0, 0, 0))
        self.window.bind('<Shift-KeyPress-Up>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, -1, 0, 0, 0, 0))
        self.window.bind('<Shift-KeyPress-Down>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 1, 0, 0, 0, 0))

        self.window.bind('<Control-KeyPress-Left>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 0, -1, 0, 0, 0))
        self.window.bind('<Control-KeyPress-Right>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 0, 1, 0, 0, 0))
        self.window.bind('<Control-KeyPress-Up>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 0, 0, -1, 0, 0))
        self.window.bind('<Control-KeyPress-Down>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 0, 0, 1, 0, 0))
        self.window.bind('<Alt-KeyPress-Left>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 0, 0, 0, -1, 0))
        self.window.bind('<Alt-KeyPress-Right>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 0, 0, 0, 1, 0))
        self.window.bind('<Alt-KeyPress-Up>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 0, 0, 0, 0, -1))
        self.window.bind('<Alt-KeyPress-Down>',
                         lambda event: self.video_capture_handler.adjust_crop(self.ui, 0, 0, 0, 0, 0, 0, 0, 1))

    def save_camera_feed(self):
        self.video_capture_handler.save_camera_feed()

    def switch_fake_camera(self):
        self.video_capture_handler.fake_camera = not self.video_capture_handler.fake_camera
        if self.video_capture_handler.fake_camera:
            self.video_capture_handler.fake_camera_video = False
        self.video_capture_handler.camera_warm_up()
        
    def switch_fake_camera_video(self):
        self.video_capture_handler.fake_camera_video = not self.video_capture_handler.fake_camera_video
        if self.video_capture_handler.fake_camera_video:
            self.video_capture_handler.fake_camera = False
        self.video_capture_handler.camera_warm_up()
        
    def switch_feedback_camera(self):
        self.feedback_camera = not self.feedback_camera

    def switch_fullscreen(self):
        if self.overrideredirect == 0:
            self.overrideredirect = 1
            self.window.overrideredirect(True)
        else:
            self.overrideredirect = 0
            self.window.overrideredirect(False)

    def save_config(self):
        self.config['CAMERA']['ID'] = str(self.camera.camera_id)
        self.config['CAMERA']['EXPOSURE'] = str(self.camera.exposure)
        self.config['CAMERA']['BRIGHTNESS'] = str(self.camera.brightness)
        self.config['CAMERA']['CONTRAST'] = str(self.camera.contrast)
        self.config['CAMERA']['FOCUS'] = str(self.camera.focus)
        self.config['CAMERA']['CAMERA_CROP_LEFT'] = str(self.video_capture_handler.camera_crop_left)
        self.config['CAMERA']['CAMERA_CROP_TOP'] = str(self.video_capture_handler.camera_crop_top)
        self.config['CAMERA']['CAMERA_CROP_RIGHT'] = str(self.video_capture_handler.camera_crop_right)
        self.config['CAMERA']['CAMERA_CROP_BOTTOM'] = str(self.video_capture_handler.camera_crop_bottom)
        self.config['CAMERA']['LEFT_PLAYAREA_OFFSET_X'] = str(self.video_capture_handler.left_playarea_offset_x)
        self.config['CAMERA']['LEFT_PLAYAREA_OFFSET_Y'] = str(self.video_capture_handler.left_playarea_offset_y)
        self.config['CAMERA']['RIGHT_PLAYAREA_OFFSET_X'] = str(self.video_capture_handler.right_playarea_offset_x)
        self.config['CAMERA']['RIGHT_PLAYAREA_OFFSET_Y'] = str(self.video_capture_handler.right_playarea_offset_y)
        with codecs.open('../config.ini', 'w', 'utf-8') as configfile:
            self.config.write(configfile)

    def init_ui(self):
        self.ui = UIData(self.config['UI'])
        self.window.geometry(f'{self.ui.width}x{self.ui.height}+{0}+{0}')

        self.ui.left.canvas = Canvas(self.window, width=self.ui.left.width, height=self.ui.left.height,
                                     bg=self.ui.background_color, highlightthickness=0, bd=0)
        self.ui.right.canvas = Canvas(self.window, width=self.ui.right.width, height=self.ui.right.height,
                                      bg=self.ui.background_color, highlightthickness=0, bd=0)
        self.ui.left.canvas.pack(side=LEFT)
        self.ui.right.canvas.pack(side=RIGHT)

        self.ui.load_masks()

        self.barchart_drawing_handler = BarPlotDrawingHandler(ui=self.ui,
                                                              nb_predictions=self.number_of_top_predictions)
        self.arc_drawing_handler = ArcDrawingHandler(ui=self.ui)

        self.phidget_manager = PhidgetManager(self.ui)

        self.ui.left.video_reader = VideoReader(90)
        self.ui.right.video_reader = VideoReader(270)
        self.video_capture_handler.compute_crop(self.ui)

        self.delay_between_frames = int(1000 / int(self.ui.fps))

    def run_loop(self):
        print('Starting the app...')
        self.window.mainloop()

    def language_menu(self):
        if len(self.ui.languages) == 1:
            self.start_menu()
            return

        self.update_on = False
        self.ui.left.player_choice_done = False
        self.ui.right.player_choice_done = False

        self.window.after(20)
        self.clear_canvas()
        self.ui.left.tk_mask = self.ui.left.canvas.create_image(0, 0,
                                                                image=self.ui.left.language_page_mask,
                                                                anchor=NW)
        self.ui.right.tk_mask = self.ui.right.canvas.create_image(0, 0,
                                                                  image=self.ui.right.language_page_mask,
                                                                  anchor=NW)
        button_commands = []
        button_texts = []
        for lang in self.ui.languages:
            button_commands.append(lambda l=lang: self.set_language(l))
            button_texts.append(self.ui.mui[lang].language)

        self.update_menu_buttons(side=self.ui.left,
                                 button_commands=button_commands,
                                 button_texts=button_texts)
        self.update_menu_buttons(side=self.ui.right,
                                 button_commands=button_commands,
                                 button_texts=button_texts)

    def set_language(self, lang: str):
        self.ui.set_lang(lang)
        self.ui.load_masks()
        self.ui.load_masks()
        self.start_menu()

    def start_menu(self):
        self.update_on = False
        self.ui.left.player_choice_done = False
        self.ui.right.player_choice_done = False

        self.window.after(20)
        self.clear_canvas()
        self.ui.left.tk_mask = self.ui.left.canvas.create_image(0, 0,
                                                                image=self.ui.left.welcome_page_mask,
                                                                anchor=NW)
        self.ui.right.tk_mask = self.ui.right.canvas.create_image(0, 0,
                                                                  image=self.ui.right.welcome_page_mask,
                                                                  anchor=NW)

        ia_button = self.ui.l.welcome_page_ia_button
        play_button = self.ui.l.welcome_page_play_button
        credits_button = self.ui.l.welcome_page_credits_button
        back_button = self.ui.l.back_button

        self.update_menu_buttons(side=self.ui.left,
                                 button_commands=[lambda: self.video_ia_choice_menu(),
                                                  lambda: self.play_menu(),
                                                  lambda: self.credits_page()],
                                 button_texts=[ia_button, play_button, credits_button])
        self.update_menu_buttons(side=self.ui.right,
                                 button_commands=[lambda: self.video_ia_choice_menu(),
                                                  lambda: self.play_menu(),
                                                  lambda: self.credits_page()],
                                 button_texts=[ia_button, play_button, credits_button])
        self.inaction_id = self.window.after(30 * 1000, self.language_menu)

    def video_ia_choice_menu(self):
        self.destroy_video_player_resources()
        self.update_on = False
        self.clear_canvas()

        self.ui.left.tk_mask = self.ui.left.canvas.create_image(0, 0,
                                                                image=self.ui.left.video_IA_choice_mask,
                                                                anchor=NW)
        self.ui.right.tk_mask = self.ui.right.canvas.create_image(0, 0,
                                                                  image=self.ui.right.video_IA_choice_mask,
                                                                  anchor=NW)

        video_1 = self.ui.l.video_ia_1_button
        video_2 = self.ui.l.video_ia_2_button
        back = self.ui.l.back_button

        self.update_menu_buttons(side=self.ui.left,
                                 button_commands=[lambda: self.start_ia_video(self.ui.l.video_IA_1, self.ui.left),
                                                  lambda: self.start_ia_video(self.ui.l.video_IA_2, self.ui.left),
                                                  lambda: self.start_menu()],
                                 button_texts=[video_1, video_2, back])
        self.update_menu_buttons(side=self.ui.right,
                                 button_commands=[lambda: self.start_ia_video(self.ui.l.video_IA_1, self.ui.right),
                                                  lambda: self.start_ia_video(self.ui.l.video_IA_2, self.ui.right),
                                                  lambda: self.start_menu()],
                                 button_texts=[video_1, video_2, back])
        self.inaction_id = self.window.after(self.inaction_time_limit * 1000, self.language_menu)

    def prepare_video_for_side(self, video, side: Side, box, mute=False):
        (x, y, w, h) = box
        side.tk_video_label = Label(side.canvas, bg=self.ui.background_color, bd=0)
        side.tk_video_container = side.canvas.create_window(x, y,
                                                            anchor=CENTER,
                                                            width=w, height=h,
                                                            window=side.tk_video_label)

        video_duration = side.video_reader.prepare_video(video, side.tk_video_label, mute)
        return video_duration

    def start_consignes_video(self, game_mode='both'):
        self.clear_canvas()

        if game_mode == 'both':
            button_commands = [lambda: self.prepare_to_start_duo_game_page(),
                               None,
                               None]
        else:
            button_commands = [lambda: self.prepare_to_start_solo_game_page(game_mode=game_mode),
                               None,
                               None]

        video_duration_l = self.prepare_consignes_video_for_side(self.ui.left, game_mode)
        video_duration_r = self.prepare_consignes_video_for_side(self.ui.right, game_mode)
        video_duration = max(video_duration_l, video_duration_r)

        if game_mode == 'left' or game_mode == 'both':
            self.ui.left.video_reader.play_video()
        if game_mode == 'right' or game_mode == 'both':
            self.ui.right.video_reader.play_video()

        self.video_timeout_id = self.window.after(video_duration,
                                                  lambda: self.ui.do_button_command(side=self.ui.left,
                                                                                    command=button_commands[0]))
        self.window.after(1000, lambda: self.prepare_consignes_video_buttons_for_side(self.ui.left,
                                                                                      game_mode, button_commands))
        self.window.after(1000, lambda: self.prepare_consignes_video_buttons_for_side(self.ui.right,
                                                                                      game_mode, button_commands))

    def prepare_consignes_video_for_side(self, side: Side, game_mode='both'):
        video_duration = 0
        if game_mode == side.name or game_mode == 'both':
            side.tk_mask = side.canvas.create_image(0, 0, image=side.video_consignes_mask, anchor=NW)

            video_duration = self.prepare_video_for_side(self.ui.l.video_consignes,
                                                         side,
                                                         (
                                                             side.video_consignes_x,
                                                             side.video_consignes_y,
                                                             self.ui.l.video_consignes_width,
                                                             self.ui.l.video_consignes_height
                                                         ),
                                                         mute=True if game_mode == 'both' and side.name == 'right' else False
                                                         )
        return video_duration

    def prepare_consignes_video_buttons_for_side(self, side: Side, game_mode='both', btn_commands=None):
        if game_mode == side.name or game_mode == 'both':
            self.update_menu_buttons(
                side=side,
                button_commands=btn_commands if self.ui.master_side == side.name else [None, None, None],
                button_texts=[
                    self.ui.l.instructions_page_skip_button if self.ui.master_side == side.name else "",
                    "",
                    ""])
        else:
            self.update_menu_buttons(side, [None, None, None], [None, None, None])

    def credits_page(self):
        self.clear_canvas()
        self.ui.left.tk_mask = self.ui.left.canvas.create_image(0, 0,
                                                                image=self.ui.left.credits_page_mask,
                                                                anchor=NW)
        self.ui.right.tk_mask = self.ui.right.canvas.create_image(0, 0,
                                                                  image=self.ui.right.credits_page_mask,
                                                                  anchor=NW)
        self.update_menu_buttons(side=self.ui.left,
                                 button_commands=[lambda: self.start_menu(), None, None],
                                 button_texts=[self.ui.l.return_welcome_button, '', ''])
        self.update_menu_buttons(side=self.ui.right,
                                 button_commands=[lambda: self.start_menu(), None, None],
                                 button_texts=[self.ui.l.return_welcome_button, '', ''])

    def start_ia_video(self, video_ia: str, side: Side):
        self.clear_canvas()
        self.ui.game_mode = 'left' if side.is_left else 'right'
        self.ui.current_side().tk_mask = self.ui.current_side().canvas.create_image(
            0, 0, image=self.ui.current_side().video_IA_mask, anchor=NW)
        self.ui.opposite_side().tk_mask = self.ui.opposite_side().canvas.create_image(
            0, 0, image=self.ui.opposite_side().video_IA_mask, anchor=NW)

        video_duration = self.prepare_video_for_side(video_ia,
                                                     self.ui.current_side(),
                                                     (
                                                         self.ui.current_side().video_IA_x,
                                                         self.ui.current_side().video_IA_y,
                                                         self.ui.l.video_IA_width,
                                                         self.ui.l.video_IA_height
                                                     ),
                                                     )
        self.prepare_video_for_side(video_ia,
                                    self.ui.opposite_side(),
                                    (
                                        self.ui.opposite_side().video_IA_x,
                                        self.ui.opposite_side().video_IA_y,
                                        self.ui.l.video_IA_width,
                                        self.ui.l.video_IA_height
                                    ),
                                    mute=True)
        self.ui.left.video_reader.play_video()
        self.ui.right.video_reader.play_video()
        self.video_timeout_id = self.window.after(video_duration, self.video_ia_choice_menu)
        self.update_menu_buttons(
            side=self.ui.current_side(),
            button_commands=[None, None, lambda: self.video_ia_choice_menu()],
            button_texts=['', '', self.ui.l.back_button])

        self.update_menu_buttons(self.ui.opposite_side(), [None, None, None], [None, None, None])

    def destroy_video_player_resources(self):
        if self.video_timeout_id is not None:
            self.window.after_cancel(self.video_timeout_id)
            self.video_timeout_id = None
        if self.ui.left.video_reader.player is not None and self.ui.left.video_reader.player.is_playing():
            self.ui.left.video_reader.stop()
        if self.ui.right.video_reader.player is not None and self.ui.right.video_reader.player.is_playing():
            self.ui.right.video_reader.stop()
        if self.ui.left.tk_video_label is not None:
            self.ui.left.tk_video_label.destroy()
            self.ui.left.canvas.delete(self.ui.left.tk_video_container)
        if self.ui.right.tk_video_label is not None:
            self.ui.right.tk_video_label.destroy()
            self.ui.right.canvas.delete(self.ui.right.tk_video_container)

    def play_menu(self):
        self.destroy_video_player_resources()
        self.clear_canvas()

        self.ui.left.tk_mask = self.ui.left.canvas.create_image(
            0, 0, image=self.ui.left.game_welcome_page_mask, anchor=NW)
        self.ui.right.tk_mask = self.ui.right.canvas.create_image(
            0, 0, image=self.ui.right.game_welcome_page_mask, anchor=NW)

        choice_1 = self.ui.l.solo_button
        choice_2 = self.ui.l.duo_button
        choice_3 = self.ui.l.back_button

        self.update_menu_buttons(self.ui.left, [lambda: self.instructions_solo('left'),
                                                lambda: self.instructions_duo('left'),
                                                lambda: self.start_menu()],
                                 [choice_1, choice_2, choice_3])

        self.update_menu_buttons(self.ui.right,
                                 [lambda: self.instructions_solo('right'),
                                  lambda: self.instructions_duo('right'),
                                  lambda: self.start_menu()],
                                 [choice_1, choice_2, choice_3])

        self.inaction_id = self.window.after(self.inaction_time_limit * 1000, self.language_menu)

    def instructions_solo(self, game_mode: str):
        self.ui.game_mode = game_mode
        self.ui.master_side = game_mode

        self.start_consignes_video(game_mode)

        self.inactive_menu()

    def instructions_duo(self, master_side: str):
        self.ui.master_side = master_side
        self.ui.left.player_ready = False
        self.ui.right.player_ready = False

        self.start_consignes_video(game_mode='both')

    def prepare_to_start_solo_game_page(self, game_mode: str):
        self.destroy_video_player_resources()
        self.clear_canvas()

        self.inactive_menu()

        if game_mode == 'left':
            side: Side = self.ui.left
        else:
            side: Side = self.ui.right

        side.tk_mask = side.canvas.create_image(
            0, 0, image=side.solo_pre_game_page_mask, anchor=NW)

        self.update_menu_buttons(
            self.ui.current_side(),
            [
                lambda: self.start_solo_game(),
                lambda: self.instructions_solo(game_mode),
                lambda: self.start_menu()
            ],
            [
                self.ui.l.pre_game_page_play_button,
                self.ui.l.pre_game_page_consignes_button,
                self.ui.l.return_welcome_button
            ])
        self.update_menu_buttons(self.ui.opposite_side(), [None, None, None], [None, None, None])
        self.inaction_id = self.window.after(self.inaction_time_limit * 1000, self.language_menu)

    def prepare_to_start_duo_game_page(self, new_game=True):
        self.destroy_video_player_resources()
        self.clear_canvas()
        self.ui.left.player_ready = False
        self.ui.right.player_ready = False

        self.ui.left.player_choice_done = False
        self.ui.right.player_choice_done = False

        self.ui.left.tk_mask = self.ui.left.canvas.create_image(
            0, 0, image=self.ui.left.duo_pre_game_page_mask, anchor=NW)
        self.update_menu_buttons(
            self.ui.left,
            [
                lambda: self.start_duo_game(self.ui.left, new_game=new_game),
                (lambda: self.instructions_duo(self.ui.master_side)) if (self.ui.master_side == 'left') else None,
                (lambda: self.start_menu()) if (self.ui.master_side == 'left') else None
            ],
            [
                self.ui.l.pre_game_page_play_button,
                self.ui.l.pre_game_page_consignes_button if (self.ui.master_side == 'left') else '',
                self.ui.l.return_welcome_button if (self.ui.master_side == 'left') else ''
            ])
        self.ui.right.tk_mask = self.ui.right.canvas.create_image(
            0, 0, image=self.ui.right.duo_pre_game_page_mask, anchor=NW)
        self.update_menu_buttons(
            self.ui.right,
            [
                lambda: self.start_duo_game(self.ui.right, new_game=new_game),
                (lambda: self.instructions_duo(self.ui.master_side)) if (self.ui.master_side == 'right') else None,
                (lambda: self.start_menu()) if (self.ui.master_side == 'right') else None
            ],
            [
                self.ui.l.pre_game_page_play_button,
                self.ui.l.pre_game_page_consignes_button if (self.ui.master_side == 'right') else '',
                self.ui.l.return_welcome_button if (self.ui.master_side == 'right') else ''
            ])

        self.inaction_id = self.window.after(self.inaction_time_limit * 1000, self.language_menu)

    def start_solo_game(self, new_game=True):
        print(f'Starting Solo Game...{new_game}')
        self.clear_canvas()
        side = self.ui.current_side()
        self.max_countdown_index = len(side.countdown_images)
        self.countdown_index = 0
        self.countdown(new_game=new_game)

    def countdown(self, new_game=True):
        countdown_time_interval = 1000
        if self.countdown_index == 0:
            if self.ui.game_mode == 'left' or self.ui.game_mode == 'both':
                self.init_countdown(side=self.ui.left)

            if self.ui.game_mode == 'right' or self.ui.game_mode == 'both':
                self.init_countdown(side=self.ui.right)

            self.countdown_index += 1
            self.update_menu_buttons(self.ui.left, [None, None, None], [None, None, None])
            self.update_menu_buttons(self.ui.right, [None, None, None], [None, None, None])
            self.window.after(countdown_time_interval, lambda: self.countdown(new_game))
        elif 0 < self.countdown_index < self.max_countdown_index:
            if self.ui.game_mode == 'left' or self.ui.game_mode == 'both':
                self.ui.left.canvas.itemconfigure(self.ui.left.tk_countdown_image,
                                                  image=self.ui.left.countdown_images[self.countdown_index])

            if self.ui.game_mode == 'right' or self.ui.game_mode == 'both':
                self.ui.right.canvas.itemconfigure(self.ui.right.tk_countdown_image,
                                                   image=self.ui.right.countdown_images[self.countdown_index])

            self.countdown_index += 1
            self.window.after(countdown_time_interval, lambda: self.countdown(new_game))

        else:
            self.clear_canvas()

            if self.ui.game_mode == 'both':
                self.init_ui_for_game(self.ui.left)
                self.init_ui_for_game(self.ui.right)

                self.update_menu_buttons(
                    self.ui.left,
                    [
                        (lambda: self.dialogue_box_retry()) if self.ui.master_side == 'left' else None,
                        (lambda: self.dialogue_box_return_welcome_menu()) if self.ui.master_side == 'left' else None,
                        None],
                    [
                        self.ui.l.retry_button if self.ui.master_side == 'left' else "",
                        self.ui.l.return_welcome_button if self.ui.master_side == 'left' else "",
                        ''
                    ])
                self.update_menu_buttons(
                    self.ui.right,
                    [
                        (lambda: self.dialogue_box_retry()) if self.ui.master_side == 'right' else None,
                        (lambda: self.dialogue_box_return_welcome_menu()) if self.ui.master_side == 'right' else None,
                        None],
                    [
                        self.ui.l.retry_button if self.ui.master_side == 'right' else "",
                        self.ui.l.return_welcome_button if self.ui.master_side == 'right' else "",
                        ''])

            else:
                self.init_ui_for_game(self.ui.current_side())
                self.inactive_menu()
                self.update_menu_buttons(
                    self.ui.current_side(),
                    [
                        lambda: self.dialogue_box_retry(),
                        lambda: self.dialogue_box_return_welcome_menu(),
                        None],
                    [
                        self.ui.l.retry_button,
                        self.ui.l.return_welcome_button,
                        ''
                    ])
                self.update_menu_buttons(self.ui.opposite_side(), [None, None, None], [None, None, None])

            if new_game:
                self.init_game()
            else:
                self.game_start_time = self.game_start_time + (time.time() - self.pause_start_time)

            self.update_on = True

            self.update_display()

    def restart_solo_game(self):
        print('restart_solo_game')
        self.start_solo_game()

    def resume_solo_game(self):
        self.start_solo_game(new_game=False)

    def cancel_next_update(self):
        if self.update_display_id is not None:
            self.window.after_cancel(self.update_display_id)

    def dialogue_box_retry(self):
        self.cancel_next_update()
        self.update_on = False
        self.pause_start_time = time.time()

        self.clear_canvas()
        if self.ui.master_side == 'left':
            side = self.ui.left
            opp_side = self.ui.right
        else:  # self.ui.master_side == 'right':
            side = self.ui.right
            opp_side = self.ui.left

        side.tk_mask = side.canvas.create_image(0, 0,
                                                image=side.dialogue_box_mask,
                                                anchor=NW)
        side.canvas.create_text(
            side.dialogue_box_text_y, side.dialogue_box_text_x,
            text=self.ui.l.popup_retry_text, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.dialogue_box_text_font, self.ui.l.dialogue_box_text_font_size),
            justify=CENTER
        )
        self.set_title_text(text=self.ui.l.waiting_for_master_player_choice_text, side=opp_side)

        if self.ui.game_mode == 'both':
            self.update_menu_buttons(side=side,
                                     button_commands=[lambda: self.restart_duo_game(),
                                                      lambda: self.resume_duo_game(), None],
                                     button_texts=[self.ui.l.yes_button, self.ui.l.no_button, ''])
            self.update_menu_buttons(self.ui.left if self.ui.master_side == 'right' else self.ui.right,
                                     [None, None, None],
                                     [None, None, None])

        else:
            self.update_menu_buttons(side=side,
                                     button_commands=[lambda: self.restart_solo_game(),
                                                      lambda: self.resume_solo_game(), None],
                                     button_texts=[self.ui.l.yes_button,
                                                   self.ui.l.no_button, ''])
            self.update_menu_buttons(self.ui.left if self.ui.master_side == 'right' else self.ui.right,
                                     [None, None, None],
                                     [None, None, None])

    def restart_duo_game(self):
        self.prepare_to_start_duo_game_page(new_game=True)

    def resume_duo_game(self):
        self.start_duo_game(self.ui.left, new_game=False)

    def dialogue_box_return_welcome_menu(self):
        self.cancel_next_update()
        self.update_on = False
        self.pause_start_time = time.time()

        self.clear_canvas()
        if self.ui.master_side == 'left':
            side = self.ui.left
            opp_side = self.ui.right
        else:  # self.ui.master_side == 'right':
            side = self.ui.right
            opp_side = self.ui.left

        side.tk_mask = side.canvas.create_image(0, 0,
                                                image=side.dialogue_box_mask,
                                                anchor=NW)
        side.canvas.create_text(
            side.dialogue_box_text_y, side.dialogue_box_text_x,
            text=self.ui.l.popup_menu_text, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.dialogue_box_text_font, self.ui.l.dialogue_box_text_font_size),
            justify=CENTER
        )
        self.set_title_text(text=self.ui.l.waiting_for_master_player_choice_text, side=opp_side)

        if self.ui.game_mode == 'both':
            self.update_menu_buttons(side=side,
                                     button_commands=[lambda: self.start_menu(),
                                                      lambda: self.resume_duo_game(), None],
                                     button_texts=[self.ui.l.yes_button, self.ui.l.no_button, ''])
        else:
            self.update_menu_buttons(side=side,
                                     button_commands=[lambda: self.start_menu(),
                                                      lambda: self.resume_solo_game(), None],
                                     button_texts=[self.ui.l.yes_button,
                                                   self.ui.l.no_button, ''])
        self.update_menu_buttons(opp_side, [None, None, None], [None, None, None])

    def init_game(self):
        self.game_over = False
        self.game_start_time = time.time()

    def init_ui_for_game(self, side: Side):
        self.update_menu_buttons(side, [None, None, None], ['', '', ''])
        side.tk_bounding_box = side.canvas.create_image(side.preview_live_y, side.preview_live_x,
                                                        image=None, anchor=CENTER)
        side.tk_cropped = side.canvas.create_image(side.preview_crop_y, side.preview_crop_x,
                                                   image=None, anchor=CENTER)
        side.tk_explanation = side.canvas.create_image(side.preview_heatmap_y, side.preview_heatmap_x,
                                                       image=None, anchor=CENTER)
        self.barchart_drawing_handler.init_bar_chart_bars(side)
        self.arc_drawing_handler.init_arc(side)
        side.tk_mask = side.canvas.create_image(0, 0,
                                                image=side.game_page_mask,
                                                anchor=NW)
        self.arc_drawing_handler.init_arc_label_and_score(side)
        self.init_timer(side)
        self.barchart_drawing_handler.init_bar_chart_labels(side)
        side.tk_cropped_side = side.canvas.create_image(side.play_area_coordinates[0],
                                                        side.play_area_coordinates[1],
                                                        image=None, anchor=NW)

        side.tk_play_area = side.canvas.create_rectangle(
            side.play_area_coordinates[0] - self.ui.l.playarea_safety_distance,
            side.play_area_coordinates[1] - self.ui.l.playarea_safety_distance,
            side.play_area_coordinates[2] + self.ui.l.playarea_safety_distance,
            side.play_area_coordinates[3] + self.ui.l.playarea_safety_distance,
            outline=self.ui.l.playarea_border_color)

    def show_wait_for_player(self, side: Side):
        self.clear_canvas(side)
        self.set_title_text(text=self.ui.l.wait_player_text, side=side)
        self.update_menu_buttons(side, [None, None, None], [None, None, None])

    def start_duo_game(self, side: Side, new_game=True):
        print('Starting Duo Game...')
        side.player_ready = True

        if self.ui.right.player_ready and self.ui.left.player_ready:
            self.ui.game_mode = 'both'

            self.clear_canvas()

            side = self.ui.left
            self.max_countdown_index = len(side.countdown_images)
            self.countdown_index = 0
            self.countdown(new_game=new_game)
        else:
            self.show_wait_for_player(side)

    def init_countdown(self, side: Side):
        side.tk_countdown_image = side.canvas.create_image(
            side.countdown_x, side.countdown_y,
            image=side.countdown_images[self.countdown_index], anchor=CENTER)

    def wait_for_other_player_to_finish(self, side: Side):
        print('Wait for other player...')

        side.player_choice_done = True
        self.ui.master_side = self.ui.right.name if side.is_left else self.ui.left.name
        if self.ui.right.player_choice_done and self.ui.left.player_choice_done:
            self.ui.left.player_choice_done = False
            self.ui.right.player_choice_done = False
            self.start_menu()
        else:
            self.show_wait_for_player(side)

    def end_game(self):
        self.clear_canvas()

        failure_text = self.ui.l.failure_text
        self.ui.left.player_ready = False
        self.ui.right.player_ready = False

        if self.ui.game_mode == 'left' or self.ui.game_mode == 'both':
            if self.ui.left.prediction_idx == -1:
                self.set_title_text(failure_text, side=self.ui.left)
                self.update_menu_buttons(self.ui.left,
                                         [lambda: self.choose_figure(self.ui.left),
                                          lambda: self.wait_for_other_player_to_finish(
                                              self.ui.left) if self.ui.game_mode == 'both' else self.start_menu(),
                                          None],
                                         [self.ui.l.yes_button, self.ui.l.no_button, ''])
            else:
                label = self.labels[self.ui.left.prediction_idx]
                solution = Image.open(os.path.join(self.ui.proposition_path, f'prop_{label}.png'))

                self.init_end_game_page(solution=solution, side=self.ui.left)
        else:
            self.update_menu_buttons(side=self.ui.left,
                                     button_commands=[None, None, None],
                                     button_texts=[None, None, None])

        if self.ui.game_mode == 'right' or self.ui.game_mode == 'both':

            if self.ui.right.prediction_idx == -1:
                self.set_title_text(failure_text, side=self.ui.right)
                self.update_menu_buttons(self.ui.right,
                                         [lambda: self.choose_figure(self.ui.right),
                                          lambda: self.wait_for_other_player_to_finish(
                                              self.ui.right) if self.ui.game_mode == 'both' else self.start_menu(),
                                          None],
                                         [self.ui.l.yes_button, self.ui.l.no_button, ''])
            else:
                label = self.labels[self.ui.right.prediction_idx]
                solution = Image.open(os.path.join(self.ui.proposition_path, f'prop_{label}.png')).rotate(180)

                self.init_end_game_page(solution=solution, side=self.ui.right)
        else:
            self.update_menu_buttons(side=self.ui.right,
                                     button_commands=[None, None, None],
                                     button_texts=[None, None, None])

        self.inaction_id = self.window.after(self.inaction_time_limit * 1000, self.language_menu)

    def init_end_game_page(self, solution, side):
        if solution is not None:
            side.solution = ImageTk.PhotoImage(
                image=solution.resize((int(self.ui.l.premiere_proposition_image_size_width),
                                       int(self.ui.l.premiere_proposition_image_size_height))))
            side.canvas.create_image(
                side.premiere_proposition_image_y,
                side.premiere_proposition_image_x,
                image=side.solution, anchor=CENTER)

        side.canvas.create_text(
            side.premiere_proposition_text_line1_y,
            side.premiere_proposition_text_x,
            text=self.ui.l.premiere_proposition_text_line1.format(side.prediction_score),
            font=(self.ui.l.premiere_proposition_text_font,
                  self.ui.l.premiere_proposition_text_font_size),
            fill=self.ui.l.premiere_proposition_text_font_color,
            angle=side.angle,
            anchor=CENTER, justify=CENTER)

        side.canvas.create_text(
            side.premiere_proposition_text_line2_y,
            side.premiere_proposition_text_x, text=self.ui.l.premiere_proposition_text_line2,
            font=(self.ui.l.premiere_proposition_text_font,
                  self.ui.l.premiere_proposition_text_font_size),
            fill=self.ui.l.premiere_proposition_text_font_color, angle=side.angle,
            anchor=CENTER, justify=CENTER)

        side.canvas.create_text(
            side.premiere_proposition_text_line3_y,
            side.premiere_proposition_text_x,
            text=self.ui.get_article_label(side.prediction_idx),
            font=(self.ui.l.premiere_proposition_label_font,
                  self.ui.l.premiere_proposition_label_font_size),
            fill=self.ui.l.premiere_proposition_label_font_color, angle=side.angle,
            anchor=CENTER, justify=CENTER)
        side.canvas.create_text(
            side.premiere_proposition_text_line4_y,
            side.premiere_proposition_text_x, text=self.ui.l.premiere_proposition_text_line4,
            font=(self.ui.l.premiere_proposition_text_font,
                  self.ui.l.premiere_proposition_text_font_size),
            fill=self.ui.l.premiere_proposition_text_font_color, angle=side.angle,
            anchor=CENTER, justify=CENTER)

        self.update_menu_buttons(side,
                                 [lambda: self.display_solution(side),
                                  lambda: self.choose_figure(side),
                                  None],
                                 [self.ui.l.yes_button, self.ui.l.no_button, ''])

    def display_solution(self, side: Side):
        self.clear_canvas(side)

        side.player_choice_done = True
        self.init_display_solution(side)
        opp_side = self.ui.right if side.is_left else self.ui.left

        if (side.name == self.ui.master_side) and (not opp_side.player_choice_done) and (self.ui.game_mode == 'both'):
            self.ui.master_side = opp_side.name
            self.update_menu_buttons(
                side,
                [None, None, None],
                ['', '', '']
            )
        else:
            self.update_menu_buttons(
                side,
                [
                    (lambda: self.display_solution_dialogue_box_retry()) if (
                            self.ui.master_side == side.name) else None,
                    (lambda: self.display_solution_dialogue_box_return_welcome_menu()) if (
                            self.ui.master_side == side.name) else None,
                    None
                ],
                [
                    self.ui.l.retry_button if (self.ui.master_side == side.name) else '',
                    self.ui.l.return_welcome_button if (self.ui.master_side == side.name) else '',
                    ''
                ])
        self.inaction_id = self.window.after(self.inaction_time_limit * 1000, self.language_menu)

    def init_display_solution(self, side):
        label = self.labels[side.prediction_idx]
        solution = Image.open(os.path.join(self.ui.solutions_path, f'{label}.png'))
        if solution is not None:
            side.solution = ImageTk.PhotoImage(image=solution if side.is_left else solution.rotate(180))
            side.canvas.create_image(side.solution_image_y, side.solution_image_x,
                                     image=side.solution, anchor=CENTER)
        side.canvas.create_text(
            side.solution_text_1_y, side.solution_text_x,
            text=self.ui.l.solution_display_text_line1, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.solution_text_font, self.ui.l.solution_text_font_size), justify=CENTER
        )
        side.canvas.create_text(
            side.solution_text_2_y, side.solution_text_x,
            text=self.ui.l.solution_display_text_line2, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.solution_text_font, self.ui.l.solution_text_font_size), justify=CENTER
        )

    def display_solution_dialogue_box_retry(self):
        if self.ui.master_side == 'left':
            side = self.ui.left
        else:  # self.ui.master_side == 'right':
            side = self.ui.right
        self.clear_canvas(side)

        side.tk_mask = side.canvas.create_image(0, 0,
                                                image=side.dialogue_box_mask,
                                                anchor=NW)
        side.canvas.create_text(
            side.dialogue_box_text_y, side.dialogue_box_text_x,
            text=self.ui.l.popup_retry_text, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.dialogue_box_text_font, self.ui.l.dialogue_box_text_font_size),
            justify=CENTER
        )

        if self.ui.game_mode == 'both':
            button_commands = [lambda: self.restart_duo_game(),
                               lambda: self.display_solution(side),
                               None]
        else:
            button_commands = [lambda: self.restart_solo_game(),
                               lambda: self.display_solution(side),
                               None]

        self.update_menu_buttons(side=side,
                                 button_commands=button_commands,
                                 button_texts=[self.ui.l.yes_button,
                                               self.ui.l.no_button,
                                               ''])

    def display_solution_dialogue_box_return_welcome_menu(self):
        if self.ui.master_side == 'left':
            side = self.ui.left
        else:  # self.ui.master_side == 'right':
            side = self.ui.right
        self.clear_canvas(side)

        side.tk_mask = side.canvas.create_image(0, 0,
                                                image=side.dialogue_box_mask,
                                                anchor=NW)
        side.canvas.create_text(
            side.dialogue_box_text_y, side.dialogue_box_text_x,
            text=self.ui.l.popup_menu_text, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.dialogue_box_text_font, self.ui.l.dialogue_box_text_font_size),
            justify=CENTER
        )

        self.update_menu_buttons(side=side,
                                 button_commands=[lambda: self.start_menu(),
                                                  lambda: self.display_solution(side), None],
                                 button_texts=[self.ui.l.yes_button, self.ui.l.no_button, ''])

    def choose_figure(self, side: Side):
        self.clear_canvas(side)

        self.clear_canvas(side)

        side.tk_mask = side.canvas.create_image(0, 0, image=side.proposition_list_mask, anchor=NW)

        if side.prediction_idx == -1:
            side.prediction_idx = 0
        side.prediction_idx = side.prediction_idx
        side.tk_label_prev = side.canvas.create_text(
            side.first_proposition_y, side.first_proposition_x,
            text=self.ui.get_article_label(side.prediction_idx - 1),
            anchor=SW,
            font=(self.ui.l.first_proposition_font, self.ui.l.first_proposition_font_size), angle=side.angle,
            fill=self.ui.l.first_proposition_font_color, justify=CENTER)
        side.tk_label_current = side.canvas.create_text(
            side.highlighted_proposition_y, side.highlighted_proposition_x,
            text=self.ui.get_article_label(side.prediction_idx),
            anchor=SW,
            font=(self.ui.l.highlighted_proposition_font, self.ui.l.highlighted_proposition_font_size), angle=side.angle,
            fill=self.ui.l.highlighted_proposition_font_color, justify=CENTER)
        side.tk_label_next = side.canvas.create_text(
            side.third_proposition_y, side.third_proposition_x,
            text=self.ui.get_article_label(side.prediction_idx + 1),
            anchor=SW,
            font=(self.ui.l.third_proposition_font, self.ui.l.third_proposition_font_size), angle=side.angle,
            fill=self.ui.l.third_proposition_font_color, justify=CENTER)
        self.update_menu_buttons(side,
                                 [lambda: self.circular_list(-1, side),
                                  lambda: self.circular_list(1, side),
                                  lambda: self.display_chosen_solution(side)],
                                 ['', '', 'OK'])
        self.circular_list(1, side)

    def circular_list(self, addition, side: Side):
        side.prediction_idx = (side.prediction_idx + addition) % len(self.labels)
        side.canvas.itemconfigure(side.tk_label_prev,
                                  text=self.ui.get_article_label(side.prediction_idx - 1)),
        side.canvas.itemconfigure(side.tk_label_current,
                                  text=self.ui.get_article_label(side.prediction_idx) + " ?"),
        side.canvas.itemconfigure(side.tk_label_next,
                                  text=self.ui.get_article_label(side.prediction_idx + 1)),

        if self.inaction_id is not None:
            self.window.after_cancel(self.inaction_id)

        self.inaction_id = self.window.after(self.inaction_time_limit * 1000, self.language_menu)

    def display_chosen_solution(self, side: Side):
        self.clear_canvas(side)
        side.player_choice_done = True
        label = self.labels[side.prediction_idx % len(self.labels)]
        solution = Image.open(os.path.join(self.ui.solutions_path, f'{label}.png'))

        opp_side = self.ui.right if side.is_left else self.ui.left

        if opp_side.player_ready:
            self.ui.master_side = side.name

        if self.ui.game_mode != 'both':
            is_master = True
        else:
            is_master = self.ui.master_side == side.name
        self.init_display_chosen_solution(side, solution)

        if (side.name == self.ui.master_side) and (not opp_side.player_choice_done) and (self.ui.game_mode == 'both'):
            self.ui.master_side = opp_side.name
            self.update_menu_buttons(
                side,
                [None, None, None],
                ['', '', '']
            )
        else:
            self.update_menu_buttons(
                side,
                [
                    (lambda: self.chosen_solution_dialogue_box_retry()) if is_master else None,
                    (lambda: self.chosen_solution_dialogue_box_return_welcome_menu()) if is_master else None,
                    None
                ],
                [
                    self.ui.l.retry_button if is_master else '',
                    self.ui.l.return_welcome_button if is_master else '',
                    ''
                ])

        self.inaction_id = self.window.after(self.inaction_time_limit * 1000, self.language_menu)

    def chosen_solution_dialogue_box_retry(self):
        if self.ui.master_side == 'left':
            side = self.ui.left
        else:  # self.ui.master_side == 'right':
            side = self.ui.right
        self.clear_canvas(side)

        side.tk_mask = side.canvas.create_image(0, 0,
                                                image=side.dialogue_box_mask,
                                                anchor=NW)
        side.canvas.create_text(
            side.dialogue_box_text_y, side.dialogue_box_text_x,
            text=self.ui.l.popup_retry_text, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.dialogue_box_text_font, self.ui.l.dialogue_box_text_font_size),
            justify=CENTER
        )

        if self.ui.game_mode == 'both':
            restart_lambda = lambda: self.restart_duo_game()
        else:
            restart_lambda = lambda: self.restart_solo_game()
        button_commands = [restart_lambda,
                           lambda: self.display_chosen_solution(side),
                           None]
        self.update_menu_buttons(side=side,
                                 button_commands=button_commands,
                                 button_texts=[self.ui.l.yes_button, self.ui.l.no_button, ''])

    def chosen_solution_dialogue_box_return_welcome_menu(self):
        if self.ui.master_side == 'left':
            side = self.ui.left
        else:  # self.ui.master_side == 'right':
            side = self.ui.right
        self.clear_canvas(side)

        side.tk_mask = side.canvas.create_image(0, 0,
                                                image=side.dialogue_box_mask,
                                                anchor=NW)
        side.canvas.create_text(
            side.dialogue_box_text_y, side.dialogue_box_text_x,
            text=self.ui.l.popup_menu_text, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.dialogue_box_text_font, self.ui.l.dialogue_box_text_font_size),
            justify=CENTER
        )

        self.update_menu_buttons(side=side,
                                 button_commands=[lambda: self.start_menu(),
                                                  lambda: self.display_chosen_solution(side),
                                                  None],
                                 button_texts=[self.ui.l.yes_button, self.ui.l.no_button, ''])

    def init_display_chosen_solution(self, side, solution):
        side.solution = ImageTk.PhotoImage(image=solution if side.is_left else solution.rotate(180))
        side.canvas.create_image(side.solution_image_y, side.solution_image_x,
                                 image=side.solution, anchor=CENTER)
        side.canvas.create_text(
            side.solution_text_1_y, side.solution_text_x,
            text=self.ui.l.display_solution_text_line1, anchor=CENTER, angle=side.angle,
            font=(self.ui.l.solution_text_font, self.ui.l.solution_text_font_size), justify=CENTER
        )
        side.canvas.create_text(
            side.solution_text_2_y, side.solution_text_x,
            text=f"{self.ui.l.display_solution_text_line2} "
                 + self.ui.get_article_label(side.prediction_idx) + " : ",
            anchor=CENTER, angle=side.angle,
            font=(self.ui.l.solution_text_font, self.ui.l.solution_text_font_size), justify=CENTER
        )

    def inactive_menu(self):
        self.set_title_text(self.ui.l.inactive_menu_text, side=self.ui.opposite_side())
        self.update_menu_buttons(self.ui.opposite_side(), [None, None, None], ['', '', ''])

    def set_title_text(self, text, side: Side = None, game_mode='both'):
        if side is None:
            if game_mode == 'both' or game_mode == 'left':
                self.set_title_text(text, side=self.ui.left)
            if game_mode == 'both' or game_mode == 'right':
                self.set_title_text(text, side=self.ui.right)
        else:
            side.canvas.create_text(side.width
                                    * ((
                                               1 - self.ui.l.title_y_ratio) if side.is_left else self.ui.l.title_y_ratio),
                                    side.height / 2,
                                    anchor=CENTER, text=text, justify=CENTER,
                                    font=(self.ui.l.title_font, self.ui.l.title_font_size),
                                    angle=side.angle,
                                    fill=self.ui.l.title_font_color)

    def update_menu_buttons(self, side: Side, button_commands: list, button_texts: list):
        side.button_commands = button_commands
        self.phidget_manager.set_leds_status(side=side)

        side.canvas.create_image(0, 0, image=side.border_mask, anchor=NW)
        if self.ui.show_buttons:
            side.button1 = Button(side.canvas, command=lambda: self.ui.do_button_command(side, 0),
                                  bg='red')
            side.button1.place(anchor=CENTER, height=self.ui.l.button_width, width=self.ui.l.button_height,
                               x=side.text_button_y[0], y=side.button_x)

            side.button2 = Button(side.canvas, command=lambda: self.ui.do_button_command(side, 1),
                                  bg='green')
            side.button2.place(anchor=CENTER, height=self.ui.l.button_width, width=self.ui.l.button_height,
                               x=side.text_button_y[1], y=side.button_x)

            side.button3 = Button(side.canvas, command=lambda: self.ui.do_button_command(side, 2),
                                  bg='yellow')
            side.button3.place(anchor=CENTER, height=self.ui.l.button_width, width=self.ui.l.button_height,
                               x=side.text_button_y[2], y=side.button_x)

        side.canvas.create_text(side.text_button_y[0], side.text_button_x,
                                anchor=E,
                                text=add_trailing_space(button_texts[0]),
                                angle=side.angle,
                                font=(self.ui.l.button_font, self.ui.l.button_font_size),
                                fill=self.ui.l.button_font_color)
        side.canvas.create_text(side.text_button_y[1], side.text_button_x,
                                anchor=E,
                                text=add_trailing_space(button_texts[1]),
                                angle=side.angle,
                                font=(self.ui.l.button_font, self.ui.l.button_font_size),
                                fill=self.ui.l.button_font_color)
        side.canvas.create_text(side.text_button_y[2], side.text_button_x,
                                anchor=E,
                                text=add_trailing_space(button_texts[2]),
                                angle=side.angle,
                                font=(self.ui.l.button_font, self.ui.l.button_font_size),
                                fill=self.ui.l.button_font_color)

    def clear_canvas(self, side: Side = None):
        if self.inaction_id is not None:
            self.window.after_cancel(self.inaction_id)
            self.inaction_id = None

        if side is None:
            self.clear_canvas(self.ui.left)
            self.clear_canvas(self.ui.right)
        else:
            if side.canvas is not None:
                side.canvas.delete('all')
            side.button_commands = [None, None, None]
            if side.button1 is not None:
                side.button1.destroy()
                side.button2.destroy()
                side.button3.destroy()

            if side.is_left:
                if len(self.barchart_drawing_handler.left_label_list) > 0:
                    self.barchart_drawing_handler.left_label_list = []
                    self.barchart_drawing_handler.left_score_list = []
                    self.barchart_drawing_handler.left_bars = []
            else:
                if len(self.barchart_drawing_handler.right_label_list) > 0:
                    self.barchart_drawing_handler.right_label_list = []
                    self.barchart_drawing_handler.right_score_list = []
                    self.barchart_drawing_handler.right_bars = []

    def update_display(self):
        if not self.update_on:
            pass
        if self.game_over:
            self.clear_canvas()

            if self.ui.game_mode == 'left' or self.ui.game_mode == 'both':
                side = self.ui.left
                side.tk_mask = side.canvas.create_image(0, 0,
                                                        image=side.time_elapsed_image_mask,
                                                        anchor=NW)
                self.update_menu_buttons(
                    side,
                    [None, None, None],
                    ['', '', '']
                )
                side.canvas.after(int(self.ui.time_out_display_duration * 1000), self.end_game)
            if self.ui.game_mode == 'right' or self.ui.game_mode == 'both':
                side = self.ui.right
                side.tk_mask = side.canvas.create_image(0, 0,
                                                        image=side.time_elapsed_image_mask,
                                                        anchor=NW)
                self.update_menu_buttons(
                    side,
                    [None, None, None],
                    ['', '', '']
                )
                side.canvas.after(int(self.ui.time_out_display_duration * 1000), self.end_game)

        else:
            start_display_ts = int(round(time.time() * 1000))
            must_predict = self.never_predicts is False and start_display_ts - self.last_predict_ts > self.delay_between_predicts
            display_content = self.video_capture_handler.get_display_content(must_predict, self.ui.game_mode)
            if must_predict:
                self.last_predict_ts = int(round(time.time() * 1000))

            if self.ui.game_mode == 'left' or self.ui.game_mode == 'both':
                preview_with_bounding_box, cropped_img, explanation, top_n_predictions = display_content['left'][1]

                self.display_previews(self.ui.left, display_content['left'][0],
                                      preview_with_bounding_box, cropped_img, explanation)

                self.display_prediction(self.ui.left, top_n_predictions)

            if self.ui.game_mode == 'right' or self.ui.game_mode == 'both':
                preview_with_bounding_box, cropped_img, explanation, top_n_predictions = display_content['right'][
                    1]

                self.display_previews(self.ui.right, display_content['right'][0],
                                      preview_with_bounding_box, cropped_img, explanation)

                self.display_prediction(self.ui.right, top_n_predictions)

            # if self.feedback_camera:
            #    cv2.imshow("all",  ImageProcessor.resize_images(display_content["all"],
            #                                                    display_content["all"].shape[0]//5,
            #                                                    display_content["all"].shape[1]//5))

            time_passed = time.time() - self.game_start_time

            seconds_left = self.game_duration - time_passed
            minutes = int(seconds_left / 60)
            seconds = int(seconds_left) % 60
            if minutes == 0 and seconds == 0:
                self.game_over = True

            else:
                self.update_timers(minutes, seconds)

            end_display_ts = int(round(time.time() * 1000))
            if self.update_on:
                self.update_display_id = self.window.after(
                    max(20, self.delay_between_frames - (end_display_ts - start_display_ts)),
                    self.update_display)

    def display_prediction(self, side: Side, top_n_predictions):
        if top_n_predictions is not None:
            self.barchart_drawing_handler.draw_bar_plot(top_n_predictions, side)
            self.barchart_drawing_handler.write_bar_plot_labels_and_scores(top_n_predictions, side)
            idx, score = sorted(top_n_predictions.items(),
                                key=lambda item: item[1], reverse=True)[0]
            side.prediction_idx = idx
            side.prediction_score = round(score * 100, 1)
            self.arc_drawing_handler.draw_arc(side, score, self.ui.get_label(side.prediction_idx))
        else:
            self.barchart_drawing_handler.reset_bar_plot(side)
            side.prediction_idx = -1
            side.prediction_score = 0
            self.arc_drawing_handler.draw_arc(side, 0, "")

    def init_timer(self, side):
        minutes = int(self.game_duration / 60)
        seconds = self.game_duration % 60
        side.tk_timer_min = side.canvas.create_text(side.timer_y, side.timer_x,
                                                    text=f'{minutes:02d} ',
                                                    anchor=E,
                                                    font=(self.ui.l.timer_font, self.ui.l.timer_font_size),
                                                    fill=self.ui.l.timer_font_color,
                                                    angle=side.angle)
        side.tk_timer_sep = side.canvas.create_text(side.timer_y, side.timer_x,
                                                    text=':',
                                                    anchor=CENTER,
                                                    font=(self.ui.l.timer_font, self.ui.l.timer_font_size),
                                                    fill=self.ui.l.timer_font_color,
                                                    angle=side.angle)
        side.tk_timer_sec = side.canvas.create_text(side.timer_y, side.timer_x,
                                                    text=f' {seconds:02d}',
                                                    anchor=W,
                                                    font=(self.ui.l.timer_font, self.ui.l.timer_font_size),
                                                    fill=self.ui.l.timer_font_color,
                                                    angle=side.angle)

    def update_timers(self, minutes: int, seconds: int):
        if self.ui.left.tk_timer_min is not None:
            self.update_timer(minutes, seconds, self.ui.left)

        if self.ui.right.tk_timer_min is not None:
            self.update_timer(minutes, seconds, self.ui.right)

    @staticmethod
    def update_timer(minutes: int, seconds: int, side: Side):
        side.canvas.itemconfigure(side.tk_timer_min, text=f'{minutes:02d} ')
        side.canvas.itemconfigure(side.tk_timer_sec, text=f' {seconds:02d}')

    def display_previews(self, side: Side, cropped_side,
                         preview_with_bounding_box, cropped_img, explanation: np.ndarray):

        if self.feedback_camera:
            cropped_side = ImageProcessor.resize_images(cropped_side,
                                                        side.play_area_coordinates[3] - side.play_area_coordinates[1],
                                                        side.play_area_coordinates[2] - side.play_area_coordinates[0]
                                                        )
            side.cropped_side = ImageTk.PhotoImage(image=Image.fromarray(cropped_side))
            side.canvas.itemconfigure(side.tk_cropped_side, image=side.cropped_side)
        else:
            side.canvas.itemconfigure(side.tk_cropped_side, image=None)

        if preview_with_bounding_box is not None:
            preview_with_bounding_box = ImageProcessor.resize_images(preview_with_bounding_box,
                                                                     self.ui.l.preview_live_width,
                                                                     self.ui.l.preview_live_height)
            side.bounding_box = ImageTk.PhotoImage(image=Image.fromarray(preview_with_bounding_box))
        else:
            side.bounding_box = None
        side.canvas.itemconfigure(side.tk_bounding_box, image=side.bounding_box)

        if cropped_img is not None:
            cropped_img = ImageProcessor.resize_images(cropped_img,
                                                       self.ui.l.preview_crop_width,
                                                       self.ui.l.preview_crop_height)
            side.cropped = ImageTk.PhotoImage(image=Image.fromarray(cropped_img))
        else:
            side.cropped = None
        side.canvas.itemconfigure(side.tk_cropped, image=side.cropped)

        if explanation is not None:
            explanation = ImageProcessor.resize_images(explanation,
                                                       self.ui.l.preview_crop_width,
                                                       self.ui.l.preview_crop_height)
            side.explanation = ImageTk.PhotoImage(image=Image.fromarray(explanation).rotate(side.angle))
        else:
            side.explanation = None
        side.canvas.itemconfigure(side.tk_explanation, image=side.explanation)
