from tkinter import E, W

from ui.ui_data import Side


class BarPlotDrawingHandler:
    def __init__(self, ui, nb_predictions):
        self.ui = ui
        self.nb_predictions = nb_predictions

        self.bar_colors = ui.l.prediction_colors

        self.left_score_list = []
        self.right_score_list = []
        self.left_bars = []
        self.right_bars = []
        self.left_label_list = []
        self.right_label_list = []

    def init_bar_chart_labels(self, side: Side):
        direction = -1 if side.is_left else 1
        score_list = self.left_score_list if side.is_left else self.right_score_list
        label_list = self.left_label_list if side.is_left else self.right_label_list
        for index in range(self.nb_predictions):
            screen_x = side.barchart_y + direction * index * self.ui.l.barchart_y_gap
            label_list.append(side.canvas.create_text(screen_x, side.barchart_label_x,
                                                      anchor=E, text='', angle=side.angle,
                                                      font=(self.ui.l.barchart_label_font,
                                                            self.ui.l.barchart_label_font_size),
                                                      fill=self.ui.l.barchart_label_font_color))
            score_list.append(side.canvas.create_text(screen_x, side.barchart_score_x, anchor=E,
                                                      text='', angle=side.angle,
                                                      font=(
                                                          self.ui.l.barchart_score_font,
                                                          self.ui.l.barchart_score_font_size),
                                                      fill=self.ui.l.barchart_score_font_color))

    def init_bar_chart_bars(self, side: Side):
        direction = -1 if side.is_left else 1
        bars = self.left_bars if side.is_left else self.right_bars
        for index in range(self.nb_predictions):
            screen_x = side.barchart_y + direction * index * self.ui.l.barchart_y_gap
            bars.append(side.canvas.create_rectangle(screen_x - int(self.ui.l.bar_height / 2), side.barchart_x,
                                                     screen_x + int(self.ui.l.bar_height / 2), side.barchart_x,
                                                     fill='white',
                                                     width=0))

    def reset_bar_plot(self, side):
        direction = -1 if side.is_left else 1
        bars = self.left_bars if side.is_left else self.right_bars
        score_list = self.left_score_list if side.is_left else self.right_score_list
        label_list = self.left_label_list if side.is_left else self.right_label_list

        for index in range(self.nb_predictions):
            screen_x = side.barchart_y + direction * index * self.ui.l.barchart_y_gap
            side.canvas.itemconfigure(score_list[index], text='')
            side.canvas.itemconfigure(label_list[index], text='')
            self.draw_bar(side.canvas, bars[index], screen_x - int(self.ui.l.bar_height / 2), side.barchart_x,
                          screen_x + int(self.ui.l.bar_height / 2), side.barchart_x,
                          'white')

    def draw_bar_plot(self, scores: dict, side):
        direction = -1 if side.is_left else 1
        bars = self.left_bars if side.is_left else self.right_bars
        for i, (label_index, score) in enumerate(sorted(scores.items(), key=lambda item: item[1], reverse=True)):
            screen_x = side.barchart_y + direction * i * self.ui.l.barchart_y_gap
            self.draw_bar(side.canvas, bars[i],
                          top_left_x=screen_x - int(self.ui.l.bar_height / 2),
                          top_left_y=side.barchart_x,
                          bottom_right_x=screen_x + int(self.ui.l.bar_height / 2),
                          bottom_right_y=side.barchart_x - direction * score * self.ui.l.bar_width,
                          color=self.bar_colors[min(int(score * (len(self.bar_colors))), 5)])

    def write_bar_plot_labels_and_scores(self, scores: dict, side):
        score_list = self.left_score_list if side.is_left else self.right_score_list
        label_list = self.left_label_list if side.is_left else self.right_label_list

        for i, (label_index, score) in enumerate(sorted(scores.items(), key=lambda item: item[1], reverse=True)):
            side.canvas.itemconfigure(score_list[i], text=f'{int(100 * score):02d} %')
            side.canvas.itemconfigure(label_list[i], text=self.ui.l.labels[label_index])

    def draw_bar(self, canvas, bar, top_left_x, top_left_y, bottom_right_x, bottom_right_y, color):
        canvas.coords(bar, top_left_x, top_left_y, bottom_right_x, bottom_right_y)
        canvas.itemconfigure(bar, fill=color)
