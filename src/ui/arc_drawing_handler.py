from tkinter import CENTER

from ui.ui_data import Side, UIData


class ArcDrawingHandler:
    def __init__(self, ui: UIData):
        self.ui = ui

    def init_arc(self, side: Side):
        side.tk_arc = side.canvas.create_arc(side.arc_center_y - self.ui.l.arc_radius,
                                             side.arc_center_x - self.ui.l.arc_radius,
                                             side.arc_center_y + self.ui.l.arc_radius,
                                             side.arc_center_x + self.ui.l.arc_radius,
                                             outline='black', fill='black', start=0, extent=0, width=0)

    def init_arc_label_and_score(self, side: Side):
        side.tk_arc_label = side.canvas.create_text(side.arc_center_y + side.arc_label_gap_y, side.arc_center_x,
                                                    anchor=CENTER, text='', angle=side.angle,
                                                    font=(self.ui.l.barchart_label_font,
                                                          self.ui.l.arc_label_font_size),
                                                    fill=self.ui.l.barchart_label_font_color)
        side.tk_arc_score = side.canvas.create_text(side.arc_center_y,
                                                    side.arc_center_x,
                                                    anchor=CENTER, text='', angle=side.angle,
                                                    font=(self.ui.l.barchart_label_font,
                                                          self.ui.l.arc_score_font_size),
                                                    fill=self.ui.l.barchart_label_font_color)

    def draw_arc(self, side: Side, score: float, label: str):
        color = self.ui.l.prediction_colors[min(int(score * (len(self.ui.l.prediction_colors))), 5)]
        side.canvas.itemconfigure(side.tk_arc, outline=color, fill=color,
                                  start=0 if side.is_left else 180,
                                  extent=(-score * 360) )
        side.canvas.itemconfigure(side.tk_arc_label, text=label)
        side.canvas.itemconfigure(side.tk_arc_score, text=f'{int(100 * score)} %')
