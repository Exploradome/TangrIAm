# <span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span>
<span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span> is an educational project to understand how Artificial Intelligence works.

The player (museum visitor) will be ask to solve a Tangram puzzle. While solving the puzzle, an computer vision based AI will predict the tangram gigure the user is doing.

![TrangrIAm gameplay](docs/tangriam-gameplay.gif)

More information on this page : https://blog.octo.com/tangriam-project-comment-expliquer-l-ia-aux-enfants/

This project is owned by [Exploradome](http://www.exploradome.fr).

It has been developed by [OCTO Technology](https://www.octo.com) with the support of [Microsoft](https://www.microsoft.com).

# Hardware requirements
- Intel i7 CPU
- RAM 8GB or 16GB
- Harddrive/SSD: 2GB of free space
- Integrated graphics card or NVidia Geforce (recommanded)
- 1080p projector
- Phidgets buttons (optional)

# OS requirements
- Windows 10 or Ubuntu (may work on other Linux distributions)

# Software development setup

See [installation instructions](docs/INSTALL.md).

# <span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span> Computer Vision workflow

![TrangrIAm Computer Vision workflow](docs/tangriam-process-ia.gif)

# Start TangrIAm app
- run ```c:\tangrIAm\TangrIAm.bat``` to start the <span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span> application 

# Troubleshooting
If it doesn't start, carefully read the error messages.
For example, you might be using the wrong camera id (check the config.ini file to change that)

See [Troubleshooting](docs/TROUBLESHOOTING.md) for more help

# Keyboard shortcuts for testing and calibration

See [Shortcuts and Configuration](docs/CONFIG.md) page.

# Improve AI performance

See [Improve performance](docs/AI_PERF_CUDA.md) page.