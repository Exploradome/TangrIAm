from unittest import TestCase

import cv2
import os
import numpy as np

from form_classification.image_processor import ImageProcessor


def compare_two_images(imageA, imageB):
    # NOTE: the two images must have the same dimension
    err = np.sum((imageA.astype('float') - imageB.astype('float')) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    return err


def get_segmented_image(data_path):
    target_segmented_img = cv2.imread(os.path.join(data_path, 'segmented/segmented.jpg'),
                                      cv2.IMREAD_GRAYSCALE)
    return target_segmented_img


def get_grey_image_processor(data_path):
    image = cv2.imread(os.path.join(data_path, 'bateau/frame-22-07-2020-19-30-33.106520.jpg'),
                       cv2.IMREAD_GRAYSCALE)
    return ImageProcessor(image, False, 224, 224)


def get_color_image_processor(data_path):
    image = cv2.imread(os.path.join(data_path, 'bateau/frame-22-07-2020-19-30-33.106520.jpg'),
                       cv2.IMREAD_COLOR)
    return ImageProcessor(image, False, 224, 224)


class TestImageProcessor(TestCase):
    def setUp(self):
        self.data_path = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'test_data')

    def test_segment_grayscale_boat_image(self):
        # Given
        image = cv2.imread(os.path.join(self.data_path, 'bateau/frame-22-07-2020-19-30-33.106520.jpg'),
                           cv2.IMREAD_GRAYSCALE)
        expected_segmented_image = get_segmented_image(self.data_path)

        # When
        segmented_img = ImageProcessor.segment_image(image)

        # Then
        for row in range(segmented_img.shape[0]):
            assert all([(element == 0 or element == 255) for element in segmented_img[row]])

        assert compare_two_images(segmented_img, expected_segmented_image) < 0.1

    def test_return_221_contours_for_boat_image(self):
        # Given
        given_segmented_image = get_segmented_image(self.data_path)

        # When
        contours = ImageProcessor.find_contours(given_segmented_image)

        # Then
        assert len(contours) == 209

    def test_draw_rectangle_on_boat_image_when_bounding_box_is_none(self):
        # Given
        x = 152
        y = 8
        height = 320
        width = 272
        image = cv2.imread(os.path.join(self.data_path, 'bateau/frame-22-07-2020-19-30-33.106520.jpg'),
                           cv2.IMREAD_GRAYSCALE)
        target_bounding_box = cv2.imread(
            os.path.join(self.data_path, 'bounding_box/frame-22-07-2020-19-30-33.106520_perfect.jpg'),
            cv2.IMREAD_GRAYSCALE)

        # When
        bounding_box = ImageProcessor.draw_rectangle(image, x, y, width, height)

        # Then
        assert compare_two_images(bounding_box, target_bounding_box) < 0.1

    def test_draw_rectangle_on_given_image(self):
        # Given
        x = 152
        y = 8
        height = 320
        width = 272
        expected_image_processor = get_color_image_processor(self.data_path)

        expected_bounding_box = cv2.imread(
            os.path.join(self.data_path, 'bounding_box/frame-22-07-2020-19-30-33.106520_colour_perfect.jpg'),
            cv2.IMREAD_GRAYSCALE)

        # When
        bounding_box = ImageProcessor.draw_rectangle(expected_image_processor.image, x, y, width, height)
        bounding_box = cv2.cvtColor(bounding_box, cv2.COLOR_BGR2GRAY)

        # Then
        assert compare_two_images(bounding_box, expected_bounding_box) < 0.1

    def test_draw_bounding_box_on_grey_boat_image(self):
        # Given
        expected_top_left_corner = (272, 152)
        expected_rectangle_height = 5
        expected_rectangle_width = 323

        given_image = cv2.imread(os.path.join(self.data_path, 'bateau/frame-22-07-2020-19-30-33.106520.jpg'),
                                 cv2.IMREAD_GRAYSCALE)
        given_image_processor = get_grey_image_processor(self.data_path)
        given_segmented_image = get_segmented_image(self.data_path)

        expected_bounding_box = cv2.imread(
            os.path.join(self.data_path, 'bounding_box/frame-22-07-2020-19-30-33.106520.jpg'),
            cv2.IMREAD_GRAYSCALE)

        # When
        max_coordinates = given_image_processor.draw_bounding_box(given_segmented_image,
                                                                  given_image,
                                                                  None,
                                                                  min_rectangle_area=0,
                                                                  max_rectangle_area=200000)
        # Then
        assert compare_two_images(given_image, expected_bounding_box) < 0.1
        assert max_coordinates[0] == expected_top_left_corner[1]
        assert max_coordinates[1] == expected_rectangle_height
        assert max_coordinates[2] == expected_top_left_corner[0]
        assert max_coordinates[3] == expected_rectangle_width
