import numpy as np

from form_classification.model import get_top_n_results


def test_return_top_3_values():
    # Given
    prediction = np.array([0.6, 0.15, 0.10, 0.65, 0.4])

    # When
    top_3_predictions = get_top_n_results(prediction, n=3)

    # Then
    assert top_3_predictions == {0: prediction[0], 3: prediction[3], 4: prediction[4]}
